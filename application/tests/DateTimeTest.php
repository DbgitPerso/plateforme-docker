<?php
/**
 * Created by PhpStorm.
 * User: m7n
 * Date: 12/10/18
 * Time: 18:40
 */

use PHPUnit\Framework\TestCase;

class DateTimeTest extends TestCase
{

    /**
     * @test
     * @throws Exception
     */
    public function shouldFormatDate()
    {

        $dateTime = new DateTime('2016-09-01');
        $interval = new DateInterval('P2D');
        $dateTime->add($interval);
        $this->assertEquals('03/09/2016', $dateTime->format('d/m/Y'));

    }
}