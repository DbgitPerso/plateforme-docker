<?php

namespace App\Controller;

use  App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    private $articleRepository;


    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }


    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $articles = $this->articleService->findAllCleanedArticle();
        return $this->render('default/home.html.twig', array(
            'articleList' => $articles,
        ));
    }
}